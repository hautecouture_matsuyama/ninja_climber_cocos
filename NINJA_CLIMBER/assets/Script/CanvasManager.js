var game = require('GameManager');

cc.Class({
    extends: cc.Component,

    properties: {

        leaderboard: cc.Node,
        adText: cc.Node,
        videoBtn: cc.Node,


        nowScore: {
            default: null,
            type: cc.Node
        },
        title: {
            default: null,
            type: cc.Node
        },
        sButton: cc.Node,
        sButton2: cc.Node,

        gameOver: {
            default: null,
            type: cc.Node
        },

        gameManager: {
            default: null,
            type: game
        },

        isStart: false,
    },

    // onLoad () {},

    //UI非表示切替タイトル画面なども含む。
    gameStart() {
        // this.title.active = false;
        if (this.isStart)
            return;

        this.isStart = true;
        this.sButton.active = false;
        this.sButton2.active = false;
        this.nowScore.active = true;

        this.gameManager.gameStart();
    },

    showLeaderboard() {
        if (this.leaderboard.active == true)
            return;
        var soundManager = cc.find('Canvas').getComponent('SoundManager');
        soundManager.enter();
        this.leaderboard.active = true;
    },

    gameEnd() {
        cc.log('終了');
        this.gameOver.active = true;

        var rand = Math.floor(Math.random() * 10);
        if (rand <= 3) {
            var adInter = cc.find('Canvas/Ads').getComponent('AdInterstitial');
            adInter.showAd(false);
        }
    },

    //広告を表示できるかどうかを判定。
    judgeIsPossible() {
        var adInter = cc.find('Canvas/Ads').getComponent('AdInterstitial');
        var adVideo = cc.find('Canvas/Ads').getComponent('AdVideo');

        //両方とも広告が取得できていない場合は広告ボタンは表示しない。
        if (!adInter.getIsPossible() && !adVideo.getIsPossible())
            this.remakeLayout();
    },

    //広告視聴後のゲームオーバー画面のレイアウトの作り直し。
    remakeLayout() {
        this.adText.active = false;
        this.videoBtn.active = false;
    },

    reStart() {
        this.gameOver.active = false;
    },

    start() {

    },

    // update (dt) {},
});

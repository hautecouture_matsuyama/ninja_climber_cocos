
//このクラスはPrefab生成も兼ねている。

var Player = require('Player');
var PlayerRot = require('PlayerRot');
var ScoreManager = require('ScoreManager');
var SoundManager = require('SoundManager');

cc.Class({
    extends: cc.Component,

    properties: {

        player: Player,
        playerRot: PlayerRot,
        camera: cc.Camera,
        scoreManager: ScoreManager,
        soundManager: SoundManager,

        startEffect: cc.ParticleSystem,

        isStart: false,
        isGameOver: false,

        spawnTime: 0,   //ブロック生成用
        afterTime: 0,   //残像生成用
    },

    getEnd() {
        return this.isGameOver;
    },
    getIsStart() {
        return this.isStart;
    },

    onLoad() {
        this.setTouchEvent();
    },

    //画面タッチイベントの設定
    setTouchEvent() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouch: true,

            onTouchBegan: function (touch, event) {
                self.setbound();
                return false;
            }
        }, self.node);
    },

    start() {
    },

    setbound() {
        if (!this.isStart)
            return;
        //ゲームオーバー画面の時はボーナス演出スキップ。
        else if (this.isGameOver && this.scoreManager.getSkiped()) {
            this.scoreManager.skip();
            return;
        }
        if (this.isGameOver)
            return;

        this.soundManager.jump();
        this.player.bound();
    },

    gameStart() {
        this.isStart = true;
        this.player.firing();
        this.player.bound();
        this.startEffect.resetSystem();
        this.scoreManager.gameStart();
        this.soundManager.changeBGM();
        this.soundManager.enter();

        //各広告の読み込み状況の確認
        var canvasManager = cc.find('Canvas').getComponent('CanvasManager');
        canvasManager.judgeIsPossible();

        setTimeout(() => {
            //指定秒後にカメラにターゲットを追加する。(パーティクル演出を最大限生かすため。)
            this.camera.addTarget(cc.find('Canvas/GameManager/Walls'));
            this.camera.addTarget(cc.find('Canvas/GameManager/Stars'));
        }, 0.5);
    },

    setGameOver() {
        this.player.damage();
        //ゲームオーバー画面を表示。
        this.soundManager.miss();
        this.isGameOver = true;
        this.scoreManager.calBonus();
        this.scoreManager.setBonus(false);
        
    },

    reloadTitle() {
        if (!this.isStart)
            return;
        this.isStart = false;
        this.soundManager.stopBGM();
        this.soundManager.enter();
        setTimeout(() => {
            cc.director.loadScene('GrabityBall');
        }, 300);
    },

    update(dt) {
        if (!this.isStart || this.isGameOver)
            return;

        var spawnSystem = cc.find('Canvas/GameManager').getComponent('SpawnSystem');
        //一定間隔で生成したブロックを配置させる。
        this.spawnTime += 1 * dt;
        if (this.spawnTime >= 0.45) {
            this.spawnTime = 0;
            spawnSystem.spawnBlock();
        }

        this.afterTime += 1 * dt;
        if (this.afterTime >= 0.05) {
            this.afterTime = 0;
            spawnSystem.afterInstantiate();
        }
    },

});

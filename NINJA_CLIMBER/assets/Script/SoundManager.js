
cc.Class({
    extends: cc.Component,

    properties: {

        menuBGM: {
            default: null,
            type: cc.AudioSource
        },

        stageBGM: {
            default: null,
            url: cc.AudioClip
        },
        bgmID: -1,

        jumpSE: {
            default: null,
            url: cc.AudioClip
        },

        bonusSE: {
            default: null,
            url: cc.AudioClip
        },

        enterSE: {
            default: null,
            url: cc.AudioClip,
        },
        missSE: {
            default: null,
            url: cc.AudioClip,
        },
    },

    // onLoad () {},

    start() {

    },

    changeBGM() {
        this.menuBGM.mute = true;
        this.bgmID = cc.audioEngine.play(this.stageBGM, true);
    },

    stopBGM() {
        cc.audioEngine.stop(this.bgmID);
        this.bgmID = -1;
    },

    jump() {
        cc.audioEngine.play(this.jumpSE,false,0.9);
    },

    bonus(vol) {
        cc.audioEngine.play(this.bonusSE,false,vol);
    },

    enter() {
        cc.audioEngine.play(this.enterSE);
    },

    miss() {
        cc.audioEngine.play(this.missSE);
    },

    // update (dt) {},
});

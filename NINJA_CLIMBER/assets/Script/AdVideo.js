
//var UNIT_ID = '349900415823399_374771620002945';

var preloadedRewardedVideo = null;

cc.Class({
    extends: cc.Component,

    properties: {

        isPossible: cc.Boolean, //表示可能か？初期化できたかどうかで値が変わる。
    },

    // onLoad () {},

    getIsPossible() {
        return this.isPossible;
    },

    start() {
        this.initialize();
    },

    //広告情報の設定
    initialize() {
        var self = this;

        //実行時(Chromeでの確認)にエラーが出ないようにする。
        if (typeof FBInstant == 'undefined') {
            return;
        }


        FBInstant.getRewardedVideoAsync(
            '534028623787304_534030747120425',
        ).then(function (rewarded) {
            self.preloadedRewardedVideo = rewarded;
            return self.preloadedRewardedVideo.loadAsync();
        }).then(function () {
            console.log('動画：初期化成功');
            this.isPossible = true;
        }).catch(function (err) {
            console.log('動画：初期化失敗')
            this.isPossible = false;
        });
    },

    //広告表示
    showAd() {
        var self = this;

        var score = cc.find('Canvas/GameManager').getComponent('ScoreManager');

        //PCかスマホかで処理を分ける。
        if (navigator.userAgent.match(/(iPhone|iPad|iPod|Android)/i)) {

            if (self.preloadedRewardedVideo == null || typeof self.preloadedRewardedVideo == 'undefined')
                return;

            self.preloadedRewardedVideo.showAsync()
                .then(function () {
                    console.log('表示成功');
                    score.setBonus(true);
                })
                .catch(function (e) {
                    console.log(e.message);
                });
        }
        //PCでは動画が表示できないので代わりにインタースティシャルを表示させる。報酬付き。
        else {
            var adInter = cc.find('Canvas/Ads').getComponent('AdInterstitial');
            adInter.showAd(true);
        }

        var canvasManager = cc.find('Canvas').getComponent('CanvasManager');
        canvasManager.remakeLayout();
    },

    // update (dt) {},
});



cc.Class({
    extends: cc.Component,

    properties: {
        player: cc.Node,
    },

    // onLoad () {},

    start() {
        this.player = cc.find('Canvas/GameManager/Player');
    },

    update(dt) {
        var game = cc.find('Canvas/GameManager').getComponent('GameManager');
        if (game.getEnd() == true)
            return;
        
        var posY = this.player.getPositionY();
        var distance = posY - this.node.position.y;
        if (distance > 800)
            this.node.active = false;
    },
});

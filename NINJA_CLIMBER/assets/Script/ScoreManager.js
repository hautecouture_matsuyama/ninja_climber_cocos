
var ScoreData = 0;
var Leaderboard = require('Leaderboard');
var SoundManager = require('SoundManager');

cc.Class({
    extends: cc.Component,

    properties: {

        leaderboard: Leaderboard,
        soundManager: SoundManager,

        nowScore: cc.Label,
        bestScore: cc.Label,

        now: 0,

        BonusText: cc.Label,    //現在のスコアの右上に出る増加量。

        bonus: true,   //ボーナス状態か。動画広告の後にtrueになる。
        isEnd: false,   //終了フラグ。スキップできるようなら有効にする。
        bonusCount: 0,
        BP: 0,

        safety: 2,      //セーフティーロック回数。数値が0で外れる。
    },

    onLoad() {

    },

    getSkiped() {
        return this.isEnd;
    },

    //スコア加算
    plusScore(value) {

        this.now += value;
        this.nowScore.string = this.now;
        if (this.now > this.getScore())
            this.setScore(this.now);
    },

    getScore() {
        var data = cc.sys.localStorage.getItem('Score');
        if (data != null)
            return Number(data);
        else
            return 0;
    },

    gameStart() {
        this.nowScore.string = this.now;
    },

    //ボーナスポイント計算
    calBonus() {
        if (!this.bonus) {
            this.sendScore();
            var canvasManager = cc.find('Canvas').getComponent('CanvasManager');
            canvasManager.gameEnd();
            return;
        }

        this.isEnd = true;

        this.BP = Math.floor(this.now / 10) * 3;         //10点につき、3点加算される。
        cc.log('現在のスコア：' + this.now);
        cc.log('ボーナス合計：' + this.BP);
        if (this.BP > 0) {
            this.BonusText.node.active = true;
            this.BonusText.string = '+' + this.BP;
        }



        //ボーナスポイント数に応じて加算速度を速くする。
        var speed = ((this.BP / 3 - 1) * 3);

        //一定速度に達したら速度変動しない。
        if (speed <= 80)
            speed = 80;

        setTimeout(() => {
            this.addBonus(speed);
        }, 300);

    },

    addBonus(speed) {

        if (this.isEnd == false) {
            cc.log('スキップ割り込み');
            return;
        }

        //一定時間ごとにボーナスポイントを1ずつ加算させる演出。

        if (this.bonusCount < this.BP && this.BP > 0) {
            setTimeout(() => {
                if (this.isEnd == false) {
                    cc.log('スキップ割り込み');
                    return;
                }
                this.soundManager.bonus();
                this.plusScore(1);
                this.bonusCount++;

                this.setCountPosition();

                this.addBonus(speed);
            }, 200 - speed);
        }
        else {
            this.BonusText.string = '';
            this.sendScore();
            var canvasManager = cc.find('Canvas').getComponent('CanvasManager');
            canvasManager.gameEnd();
        }
    },

    setCountPosition(end) {

        //桁数に応じて表示位置を変える。
        // var nStr = String(this.now);
        // var x = 225 + (nStr.length - 1) * 60;

        // {// //未使用。現在のスコアに'1'が含まれていた場合も位置の調整を行う。1が含まれている場合、表示がかなりずれるため
        //     // var targetStr = '1';
        //     // var oneCount = (nStr.match(new RegExp(targetStr, 'g')) || []).length;
        //     // x -= oneCount * 25;
        // }
        // this.BonusText.node.setPosition(x, 265);

        this.BonusText.string = '+' + (this.BP - this.bonusCount);
        if (end)
            this.BonusText.string = '';
    },

    skip() {
        if (!this.bonus)
            return;

        this.safety--;
        if (this.safety > 0) {
            cc.log('残りロック回数：' + this.safety);
            return;
        }

        cc.log('スキップ');
        this.soundManager.bonus();
        this.isEnd = false;
        var value = this.BP - this.bonusCount;
        this.bonusCount = this.BP;
        this.plusScore(value);
        this.setCountPosition(true);
        this.sendScore();
        var canvasManager = cc.find('Canvas').getComponent('CanvasManager');
        canvasManager.gameEnd();
    },

    getBonus() {
        var isBonus = cc.sys.localStorage.getItem('boolBonus');
        var b;
        if (isBonus == 1) {
            b = true;
        }
        else {
            b = false;
        }

        return b;
    },

    setBonus(value) {
        this.bonus = value;
        if (value == true)
            cc.sys.localStorage.setItem('boolBonus', 1);
        else{
            cc.sys.localStorage.setItem('boolBonus', 0)
        }
    },

    //記録更新時に呼ばれる。
    setScore(score) {
        cc.sys.localStorage.setItem('Score', score);    //オフラインの記録保存。
        this.bestScore.string = 'BEST: ' + score;
    },

    sendScore() {
        var score = this.getScore();
        console.log(score)
        this.leaderboard.sendScore(score);

    },

    start() {
        this.bonus = this.getBonus();
        this.bestScore.string = 'BEST: ' + this.getScore();
    },



    // update (dt) {},
});

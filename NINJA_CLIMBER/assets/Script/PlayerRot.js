

cc.Class({
    extends: cc.Component,

    properties: {
        child: cc.Node,
    },

    // onLoad () {},

    start() {
    },

    rotate(isLeft) {
        var rotZ = isLeft ? 180 : 0;
        this.child.rotationY += 180;
        var rot = cc.rotateTo(0.15, rotZ);
        this.node.runAction(rot);
    },

    // update(dt) {},
});


var PlayerRot = require('PlayerRot');

cc.Class({
    extends: cc.Component,

    properties: {

        isLeft: true,        //移動方向。trueが左方向。
        isEnter: true,       //同じ壁で処理を起こさせないようにする。

        playerRot: PlayerRot,

        body: {
            default: null,
            type: cc.RigidBody
        },
        
    },

    onEnable() {
        cc.director.getPhysicsManager().enabled = true;
    },

    // onLoad () {},

    getDir() {
        return this.isLeft;
    },

    start() {
    },

    climb() {

        var moveY = cc.moveBy(0.4, 0, 500);
        return cc.repeatForever(moveY);
    },
    firing() {
        this.node.rotation = 270;
        this.sequence = this.climb();
        this.node.runAction(this.sequence);
    },

    bound() {
        var game = cc.find('Canvas/GameManager').getComponent('GameManager');
        if (game.getEnd() == true)
            return;
        
        this.playerRot.rotate(this.isLeft);
        
        //左向きなら右に移動。
        if (this.isLeft) {
            this.body.linearVelocity = cc.p(-10000, 0);
            this.isLeft = false;
        }
        else {
            this.body.linearVelocity = cc.p(10000, 0);
            this.isLeft = true;
        }

        setTimeout(() => {
            this.isEnter = false;
        }, 50);
    },

    damage() {
        this.node.stopAllActions();
        this.body.gravityScale = 3;
    },

    onBeginContact(contact, self, other) {
        var game = cc.find('Canvas/GameManager').getComponent('GameManager');
        if (game.getEnd() == true)
            return;

        var game = cc.find('Canvas/GameManager');
        var camera = cc.find('Canvas/GameManager/Camera').getComponent('CameraControl');
        var score = cc.find('Canvas/GameManager').getComponent('ScoreManager');

        var otherName = other.node.name;
        if (otherName == 'LeftWall') {
            if (!this.isEnter) {
                this.isEnter = true;
                score.plusScore(1);
                //衝突時のパーティクルを生成。
                game.getComponent('SpawnSystem').spawnParticle(true,this.node.position.y);
                //画面を揺らす。
                camera.shake();
            }
            
        }
        else if (otherName == 'RightWall') {
            if (!this.isEnter) {
                this.isEnter = true;
                score.plusScore(1);
                //衝突時のパーティクルを生成。
                game.getComponent('SpawnSystem').spawnParticle(false,this.node.position.y);
                //画面を揺らす。
                camera.shake();
            }
        }
        else if (otherName == 'BlockA' || otherName == 'BlockB') {

            game.getComponent('GameManager').setGameOver();
        }
    },

    // update(dt) {},


});



cc.Class({
    extends: cc.Component,

    properties: {

        //位置情報再設定の際に使うランダム値
        minY: 700,
        maxY: 1050,
        minX: -265,
        maxX: 265,


        isStart: false,
        logTime: 0,
        player: cc.Node,
    },

    // onLoad () {},

    start() {
        this.player = cc.find(('Canvas/GameManager/Player'))
    },

    update(dt) {

        // this.logTime += 1 * dt;
        // if (this.logTime >= 1) {
        //     this.logTime = 0;
        //     cc.log(this.node.position);
        // }

        var game = cc.find('Canvas/GameManager').getComponent('GameManager');
        if (game.getEnd() == true)
            return;

        var posY = this.player.getPositionY();
        var distance = posY - this.node.position.y;
        if (distance > 700) {
            var randX = Math.floor(Math.random() * (this.maxX + 1 - this.minX)) + this.minX;
            var randY = Math.floor(Math.random() * (this.maxY + 1 - this.minY)) + this.minY;
            this.node.position = cc.p(randX, posY + randY);
        }
    },

    
});

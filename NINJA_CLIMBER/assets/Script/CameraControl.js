

cc.Class({
    extends: cc.Component,

    properties: {
        target: cc.Node,
        camera: cc.Camera,

        logTime: 0,
    },

    // onLoad () {},

    start () {

    },

    shake() {
        var game = cc.find('Canvas/GameManager').getComponent('GameManager');
        if (game.getEnd() == true)
            return;
        
        var shakeL = cc.moveBy(0.02, -8, 0);
        var shakeR = cc.moveBy(0.02, 8, 0);

        this.node.runAction(cc.sequence(shakeL, shakeR));
    },

    update(dt) {

        // this.logTime += 1 * dt;
        // if (this.logTime >= 0.5) {
        //     this.logTime = 0;
        //     cc.log('カメラの位置情報' + this.node.position);
        //     cc.log('プレイヤーの位置情報' + this.target.getPosition());
        // }

        var game = cc.find('Canvas/GameManager').getComponent('GameManager');
        if (game.getEnd() == true)
            return;

        var newPos = this.target.position;
        this.node.position = cc.p(0, newPos.y);
    },
});



cc.Class({
    extends: cc.Component,

    properties: {

    },

    // onLoad () {},

    onEnable() {
        var effect = this.node.getComponent(cc.ParticleSystem);
        effect.resetSystem();

        setTimeout(() => {
            this.node.active = false;
        }, 500);
    },

    start () {

    },

    // update (dt) {},
});

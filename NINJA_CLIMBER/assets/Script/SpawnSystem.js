
const minX = -145;
const maxX = 145;

const BminX = 45;
const BmaxX = 325;

cc.Class({
    extends: cc.Component,

    properties: {

        prefabA: cc.Prefab,
        prefabB: cc.Prefab,
        crashL: cc.Prefab,
        crashR: cc.Prefab,
        afterPrefab: cc.Prefab,

        blockParent: cc.Node,
        particleParent: cc.Node,
        afterParent: cc.Node,
        player: cc.Node,
        playerRot: cc.Node,

        particleL_BOX: {
            default: [],
            type: cc.Node
        },
        particleR_BOX: {
            default: [],
            type: cc.Node
        },
        //生成したブロックの格納先。
        blockA_BOX: {
            default: [],
            type: cc.Node
        },
        blockB_BOX: {
            default: [],
            type: cc.Node
        },
    },

    onLoad() {

        while (Object.keys(this.blockA_BOX).length < 20) {
            this.blockA_BOX.push(this.doInstantiate(this.prefabA, this.blockParent));
        }
        while (Object.keys(this.blockB_BOX).length < 20) {
            this.blockB_BOX.push(this.doInstantiate(this.prefabB, this.blockParent));
        }

        while (Object.keys(this.particleL_BOX).length < 2) {
            this.particleL_BOX.push(this.doInstantiate(this.crashL, this.particleParent));
        }
        while (Object.keys(this.particleR_BOX).length < 2) {
            this.particleR_BOX.push(this.doInstantiate(this.crashR, this.particleParent));
        }
    },

    start() {

    },

    //ブロックの生成。
    doInstantiate(obj, parent) {
        var o = cc.instantiate(obj);
        o.parent = parent;
        o.active = false;
        return o;
    },

    spawnParticle(isLeft, posY) {

        if (isLeft) {
            if (Object.keys(this.particleL_BOX).length > 0) {
                var pa = this.particleL_BOX.filter(o => o.activeInHierarchy == false && o.name == 'LeftCrash');

                //もしなかったら新しく作る。
                if (pa == null || Object.keys(pa).length == 0) {
                    this.particleL_BOX.push(this.doInstantiate(this.crashL, this.particleParent));
                    return this.spawnParticle(isLeft);
                }

                pa[0].active = true;
                pa[0].setPosition(cc.p(-265, posY));

                return;
            }
            this.particleL_BOX.push(this.doInstantiate(this.crashL, this.particleParent));
            return this.spawnParticle(isLeft);
        }
        else {
            if (Object.keys(this.particleR_BOX).length > 0) {
                var pa = this.particleR_BOX.filter(o => o.activeInHierarchy == false && o.name == 'RightCrash');

                //もしなかったら新しく作る。
                if (pa == null || Object.keys(pa).length == 0) {

                    this.particleR_BOX.push(this.doInstantiate(this.crashR, this.particleParent));
                    return this.spawnParticle(isLeft);
                }

                pa[0].active = true;
                pa[0].setPosition(cc.p(265, posY));

                return;
            }
            this.particleR_BOX.push(this.doInstantiate(this.crashR, this.particleParent));
            return this.spawnParticle(isLeft);
        }
    },

    spawnBlock() {
        //どのブロックかはランダム。
        var rand = Math.floor(Math.random() * 2);       //AとBのどちらか。0がA

        var posY = this.player.getPositionY() + 1500;
        var posX = Number;

        //Aブロック
        if (rand == 0) {
            posX = Math.floor(Math.random() * (maxX * 1 - minX)) + minX;

            if (Object.keys(this.blockA_BOX).length > 0) {
                var b = this.blockA_BOX.filter(o => o.activeInHierarchy == false);

                if (b == null || Object.keys(b).length == 0) {
                    this.blockA_BOX.push(this.doInstantiate(this.prefabA, this.blockA_BOX));
                    return this.spawnBlock();
                }

                b[0].active = true;
                b[0].setPosition(cc.p(posX, posY));
                return;
            }
            this.blockA_BOX.push(this.doInstantiate(this.prefabA, this.blockA_BOX));
            return this.spawnBlock();
        }
        //Bブロック
        else {
            posX = Math.floor(Math.random() * (BmaxX * 1 - BminX)) + BminX;
            var rand2 = Math.floor(Math.random() * 2);      //左右のどちら側か。0が左

            if (rand2 == 0)
                posX *= -1;

            if (Object.keys(this.blockB_BOX).length > 0) {
                var b = this.blockB_BOX.filter(o => o.activeInHierarchy == false);

                if (b == null || Object.keys(b).length == 0) {
                    this.blockB_BOX.push(this.doInstantiate(this.prefabB, this.blockB_BOX));
                    return this.spawnBlock();
                }

                b[0].active = true;
                b[0].setPosition(cc.p(posX, posY));
                return;
            }
            this.blockB_BOX.push(this.doInstantiate(this.prefabB, this.blockB_BOX));
            return this.spawnBlock();
        }

    },

    //残像を生成。
    afterInstantiate() {
        var af = cc.instantiate(this.afterPrefab);
        af.setPosition(this.player.getPosition());
        af.rotation = this.playerRot.rotation + 270;
        af.parent = this.afterParent;
    }

    // update (dt) {},
});

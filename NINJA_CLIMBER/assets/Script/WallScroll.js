

cc.Class({
    extends: cc.Component,

    properties: {
        player: cc.Node,
    },

    // onLoad () {},

    start () {
    },

    update(dt) {
        var game = cc.find('Canvas/GameManager').getComponent('GameManager');
        if (game.getEnd() == true)
            return;
        
        var posY = this.player.getPositionY();
        var distance = posY - this.node.getPositionY();
        if (distance > 1200) {
            //壁2個分上に移動する。
            this.node.setPosition(this.node.getPositionX(), this.node.getPositionY() + 2800);
        }
    },
});

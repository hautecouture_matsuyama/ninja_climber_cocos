

cc.Class({
    extends: cc.Component,

    properties: {
        
    },

    // onLoad () {},

    onEnable() {
        var player = cc.find('Canvas/GameManager/Player').getComponent('Player');
        if (player.getDir() == false) {
            this.node.rotationY += 180;
        }
            
        this.node.opacity = 200;
    },

    start () {
    },

    update(dt) {
        this.node.opacity -= 255 * (dt * 1.5);

        if (this.node.opacity <= 1) {
            this.node.destroy();
        }
    },
});
